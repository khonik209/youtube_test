<?php

namespace App\Http\Controllers;


use Alaouy\Youtube\Facades\Youtube;
use Illuminate\Http\Request;

class YoutubeController extends Controller
{
    /**
     * Страница со списком видео канала (макс. 50).
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getVideos(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
        ]);
        $channel = Youtube::getChannelByName($request->username, false, ['id', 'snippet', 'contentDetails', 'statistics']);
        if (!$channel) {
            return back(); // todo: возвращать с ошибкой. back()->with();
        }
        $videos = Youtube::listChannelVideos($channel->id, 50, 'date');
        return view('videos', compact('videos'));
    }
}
